# process-injector

Simple process injector which injects any dll files

<img src="img/default.PNG">


<h1>Example:</h1>
<img src="img/test.PNG">
<img src="img/dllinjection.PNG">

<h1>Requirements:</h1>
<ul>
    <li><b>Qt</b></li>
    <li><b>Win X64</b></li>
</ul>