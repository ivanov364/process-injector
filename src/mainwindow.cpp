#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <windows.h>
#include <iostream>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->lineEdit->setPlaceholderText("Enter PID");
}

MainWindow::~MainWindow()
{
    delete ui;
}

QString filePath; // global variable for file path


void MainWindow::on_pushButton_clicked() /* BROWSE FOR DLL */
{

    filePath = QFileDialog::getOpenFileName(this, "Select your file", "C://", "Dll files (*.dll);;");


    ui->textBrowser->append("Selected DLL: " + filePath);

}

void MainWindow::on_pushButton_2_clicked() /* INJECT INTO PROCESS */
{

    HANDLE processHandle;
    PVOID remoteBuffer;

    wchar_t dllPath[8096];
    int length = filePath.left(8096 - 1).toWCharArray(dllPath);
    dllPath[length] = '\0';

    QString pid2 = ui->lineEdit->text();
    int pid = pid2.toInt();

    ui->textBrowser->append("Process ID: " + QString::number(pid)); // pid2

    processHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, DWORD(pid));
    remoteBuffer = VirtualAllocEx(processHandle, NULL, sizeof(dllPath), MEM_COMMIT, PAGE_READWRITE);
    WriteProcessMemory(processHandle, remoteBuffer, (LPVOID)dllPath, sizeof(dllPath), NULL);
    PTHREAD_START_ROUTINE threatStartRoutineAddress = (PTHREAD_START_ROUTINE)GetProcAddress(GetModuleHandle(TEXT("Kernel32")), "LoadLibraryW");
    CreateRemoteThread(processHandle, NULL, 0, threatStartRoutineAddress, remoteBuffer, 0, NULL);
    CloseHandle(processHandle);

    ui->textBrowser->append("Injected!");

}

